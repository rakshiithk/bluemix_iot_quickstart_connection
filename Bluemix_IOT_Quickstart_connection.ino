#include <Bridge.h>
#include <YunServer.h>
#include <YunClient.h>
#include <PubSubClient.h>

YunServer server;
YunClient yunClient;
boolean broadcast = true;
const int trigPin = 2;
const int echoPin = 3;
const int tempPin = 0;
const int lightPin = 1;
PubSubClient mqtt("quickstart.messaging.internetofthings.ibmcloud.com", 1883, callback, yunClient);
unsigned long time;
int value;
float temp;
String pubString;
char pubChars[50];
char username[]="use-token-auth";
char password[]="&&G70_yD0o-_BWUJ5?";

void setup()
{
  pinMode(trigPin,OUTPUT);
  pinMode(echoPin,INPUT);
  Bridge.begin();
  mqtt.connect("d:quickstart:sensors:b4218af00a93");
  server.listenOnLocalhost();
  server.begin();
}

void loop()
{
  YunClient client = server.accept();
  if(client){
    String command = client.readString();
    command.trim();
    if(command == "start")
      broadcast = true;
    if(command == "stop")
      broadcast = false; 
     client.print("Status of the Device has been set");
    client.stop(); 
  }
  if (millis() > (time + 2000) && broadcast)
  {
    time = millis();
    value = analogRead(tempPin);
    temp = 23.2;
    
//    pubString = "{\"d\":{\"temp\":" + String(temp);
//    value = analogRead(lightPin);
//    pubString += ",\"light\":" + String(value) + "}}";
    pubString = "{\"d\":{\"distance\":" + String(getDistance())  + "}}";
    
    pubString.toCharArray(pubChars, pubString.length() + 1);
    mqtt.publish("iot-2/evt/status/fmt/json", pubChars);
  }
  mqtt.loop();
}

int getDistance(){
  digitalWrite(trigPin,LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(5);
  digitalWrite(trigPin,LOW);
  long duration = pulseIn(echoPin, HIGH);
  return duration / 29 / 2;
}

void callback(char* topic, byte* payload, unsigned int length) { }

